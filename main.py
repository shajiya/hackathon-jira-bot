import yaml
import sys
from test_frameworks.pytest.pytest_json_parser import pytest_result_parser
from test_frameworks.robot.robot_xml_parser import robot_result_parser
import jira_methods
from test_error_filter import *

def user_input():
	with open("jira_bot_user.yml" , "r") as stream:
	    try:
	        return yaml.safe_load(stream)
	    except yaml.YAMLError as exc:
	        print(exc)

jira_obj = jira_methods.JiraMethods()
user_dict = user_input()

def error_filter(err_msg):
	for line in err_msg.splitlines():
		for string in line.split():
			if string.endswith('Error:'):
				err_type = string.strip(':')
				if valid_error_filter(err_type):
					return True
	return False

def get_previous_jira(label = None):
	'''
	filter the issue in jira database based on the labels and returns jira id.
    *Parameters:* label is list of labels that is unique to each issue. Based on these labels, it will filter issues.
    *Return:* Returns jira ID if issue is found or else it will return None
	'''
	if label is not None:
		# print('status not in (Done) AND labels = {} AND labels = {} AND labels = {}'.format(*label))
		old_jira = jira_obj.query_jira('status not in (Done) AND labels = {} AND labels = {} AND labels = {}'.format(*label))

		if len(old_jira) > 1:
			print('Error: Seen more than one issue with the given labels.Manually update single issue with the failed suites and close duplicate issues ')
			exit()
		elif old_jira:
			print('Existing issue found in JIRA: ', str(old_jira[0].key))
			return str(old_jira[0].key)                           
		else:
			return None

def create_new_jira(summary, description, label=None):
	'''
	This is a method for creation of customized JIRA for ONT product.It just creates an issue with the fields given and return it's jira ID
    
    *Parameters:* 
    | *Key*               | *Value*                                   |
    |  *summary*          | Summary of the issue                      |
    |  *description*      | Description of the issue 			      |
    |  *label*            | list of labels. This is must need identify the issue and to update the issue in future |
    *Return:*  jira ID of the created issue
	'''
	if all([summary, description, label]) :
		issue_dict = {
		"project"    				: user_dict['project'],
		"issuetype"                 : user_dict['issuetype'],
		"summary"					: summary,
		"description"               : description,
		'customfield_11392'			: user_dict['team_name'],
		"labels"                    : label,
		}

		new_jira = jira_obj.create_jira(issue_dict=issue_dict)
		return new_jira

	else:
		print('Error: Unable to create jira as some mandatory fields are not filled')

def main(test_report):
	'''
	This method will handle the process of automatic issue creation and updation.
	steps:
	-> It parses the test report file and divides the failed and passed suites.
	-> Then for failed suites it will check if any previuosly created issues
	-> If any previous issue found, then will just comment with more info and attach the required log files.
	-> If no previous issue found, then call create_new_jira() method. And then attaches the required log for new issue
	-> Then for passed suites, it will check if any previuosly created issues
	-> If any previous issue found, it will just attach the required log and closes that issue
    
    *Parameters:* 
    | *Key*               | *Value*                                   |
    |  *test_report*      | path to test report files                 |
    *Return:* None
	'''
	platform = user_dict['platform']
	build = user_dict['build']

	try:
		if user_dict['test_framework_type'] == "robotframework":
			stats = robot_result_parser(test_report)
		if user_dict['test_framework_type'] == "pytest":
			stats = pytest_result_parser(test_report)
	except Exception as e:
		print(e)
		exit()

	if stats['failed_suites']:

		print(f'found failed suites')
		# print(stats['failed_suites'])

		for each in stats['failed_suites']:
			flabel = ['JIRA_BOT', list(each.keys())[0].replace(" ", "_"), platform]
			prev_jira = get_previous_jira(flabel)
			if prev_jira is not None:
				print('trying to update existing issue', prev_jira)
				add_comment = '\n' + 'build version: '+ build + '\n' + 'Failed Tests:\n'
				for test in list(each.values())[0]:
					if test[1] in ('FAIL', 'failed') :
						err_msg = "".join([s for s in str(test[2]).strip().splitlines(True) if s.strip()])
						if not error_filter(err_msg):
							add_comment += '|' + test[0] + '|' + err_msg +'|' + '\n'
						else:
							print(f"{test[0]} has invalid error and hence not adding to jira comment")
				if len(add_comment.splitlines()) > 3:
					jira_obj.update_jira(jira_id = prev_jira, comment = add_comment)
				else:
					print(f"{list(each.keys())[0]} has no valid issue and hence not adding to jira comment")
			else:
				print('Creating new issue on jira')
				summary = 'JIRA_BOT : ' + platform + ' ' + list(each.keys())[0] + ' Failed'
				description = 'Unit Under Test: ' + platform  + '\n' + 'suite: ' + list(each.keys())[0] + '\n\n' +\
							  'For detailed information refer tables in comment section.\n' +\
							  'column1 gives list of testcases and column2 gives their failure reason' 
				first_comment = 'build version: '+ build + '\n' + 'Failed tests : ' + '\n'
				for test in list(each.values())[0]:
					if test[1] in ('FAIL', 'failed'):
						err_msg = "".join([s for s in str(test[2]).strip().splitlines(True) if s.strip()])
						if not error_filter(err_msg):
							first_comment += '|' + test[0] + '|' + err_msg +'|' + '\n'
						else:
							print(f"{test[0]} has invalid error and hence not added to jira comment")
				# print(summary)
				# print(description)
				# print(first_comment)
				if len(first_comment.splitlines()) > 3:
					new_jira_id = create_new_jira(summary=summary, description=description, label = flabel)
					if new_jira_id:
						jira_obj.update_jira(jira_id = new_jira_id, comment = first_comment)
				else:
					print(f"{list(each.keys())[0]} has no valid issue and not created issue")
	else:
		print('no failed suites were found')

	if stats['passed_suites']:
		print('found passed_suites')
		# print(stats['passed_suites'])

		for each in stats['passed_suites']:
			plabel = ['JIRA_BOT', list(each.keys())[0].replace(" ", "_"), platform]
			prev_jira2 = get_previous_jira(plabel)
			if prev_jira2 is not None:
				print('trying to close previous jira', prev_jira2)
				pass_comment = 'All tests passed in build version: ' + build
				jira_obj.update_jira(jira_id = prev_jira, comment = pass_comment)			
	else:
		print('no passed suites were found')

if __name__ == '__main__':
	# pass
	# main("C:\\Users\\thumbur\\OneDrive - F5 Networks\\hackathon-jira-bot\\test_frameworks\\robot\\sample_test_reports\\final_output.xml")
	# main("C:\\Users\\thumbur\\OneDrive - F5 Networks\\hackathon-jira-bot\\test_frameworks\\pytest\\sample_test_reports\\log.json")
	main(sys.argv[1])
