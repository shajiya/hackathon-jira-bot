import json
import sys

node_list = []
suite_list = []
passed_suites = []
failed_suites = []
result_dict = dict()
temp_location = None

def result_visitor():
	for suite in suite_list:
		result_list = []
		for node in node_list:
			if suite in node['location'][0] and (node["$report_type"] == "TestReport") and (node['when'] != 'setup') and (node['when'] != 'teardown'):
				result_list.append(node['outcome'])
		# print(suite, result_list)
		if result_list.count('skipped') == len(result_list):
			result_dict[suite] = 'skipped'
		if result_list.count('failed') >= 1:
			result_dict[suite] = 'failed'
		if (result_list.count('passed') >= 1) and (result_list.count('failed') == 0) :
			result_dict[suite] = 'passed'

def suite_name_exists(suite_name, suite_list):
	# print(suite_name, suite_list)
	for suite in suite_list:
		if suite_name in suite.keys():
			return suite_list.index(suite)

def error_msg_get(node_id):
	with open('log.json') as f:
		for json_obj in f:
			node = json.loads(json_obj)
			if ('location' in node) and (node["$report_type"] == "TestReport") and (node['when'] == 'teardown'):
				return node['when']

def pytest_result_parser(json_file):

	with open(json_file) as f:
		for json_obj in f:
			node = json.loads(json_obj)
			if ('location' in node) and (node["$report_type"] == "TestReport") and (node['when'] != 'setup') and (node['when'] != 'teardown'):
					node_list.append(node)
					suite_name = node['location'][0].split('/')[-1].split('.py')[0]
					if suite_name not in suite_list: 
						suite_list.append(suite_name)
	result_visitor()
	# print(result_dict)

	for suite in suite_list:
		for node in node_list:
			if (suite in node['location'][0]) and (node['when'] != 'setup') and (node['when'] != 'teardown'):

				test_name = node['nodeid'].split('/')[-1].split('::')[1:]
				if len(test_name) > 1:
					test_name = '::'.join(test_name)
				temp_suite_list = []

				if failed_suites:
					for suite_dict in failed_suites:
						temp_suite_list.append(suite_dict)
				if result_dict[suite] == 'failed':
					if node['outcome'] == 'failed':
						suite_index = suite_name_exists(suite, temp_suite_list)
						if suite_index is None:						
							failed_suites.append({suite : [(test_name, node['outcome'],node['longrepr']['reprcrash']['message'])]})
							# failed_suites.append({suite : [(test_name, node['outcome'])]})
						else:
							failed_suites[suite_index][suite].append((test_name, node['outcome'],node['longrepr']['reprcrash']['message']))
							# failed_suites[suite_index][suite].append((test_name, node['outcome']))
					if node['outcome'] == 'passed':
						suite_index = suite_name_exists(suite, temp_suite_list)
						if suite_index is None:							
							failed_suites.append({suite : [(test_name, node['outcome'])]})
						else:
							failed_suites[suite_index][suite].append((test_name, node['outcome']))

				if passed_suites:
					for suite_dict in passed_suites:
						temp_suite_list.append(suite_dict)
				if result_dict[suite] == 'passed':
					suite_index = suite_name_exists(suite, temp_suite_list)
					if suite_index is None:							
						passed_suites.append({suite : [(test_name, node['outcome'])]})
					else:
						passed_suites[suite_index][suite].append((test_name, node['outcome']))
	# 	print('-'*100)
	# print(passed_suites, len(passed_suites))
	# print('#'*100)
	# print(failed_suites, len(failed_suites))				
	return {'passed_suites': passed_suites , 'failed_suites' : failed_suites}


if __name__ == '__main__':
	# print(pytest_result_parser("C:\\Users\\thumbur\\OneDrive - F5 Networks\\hackathon-jira-bot\\test_frameworks\\pytest\\sample_test_reports\\log.json"))
	pytest_result_parser(sys.argv[1])

