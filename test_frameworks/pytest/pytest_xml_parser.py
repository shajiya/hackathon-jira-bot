import os
from junitparser import JUnitXml, Failure, Skipped, Error
import tempfile
import subprocess

def main():

    xml = JUnitXml.fromfile('report.xml')
    for suite in xml:
        print(suite)
        for case in suite:
            #print(type(case))
            print(case.name)
            if case.result:
                print(case.result)
                if isinstance(case.result, Failure):
                    print('  failure ', case.result.message)
                if isinstance(case.result, Skipped):
                    print('  skipped ', case.result.message)

main()